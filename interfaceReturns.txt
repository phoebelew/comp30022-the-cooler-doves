**********PlayerCurrencyInterface

AddCoins: returns true if success, false if failure
RetrieveCoins: returns # of coins if success, -1 if failure

**********PlayerScoreInterface

AuthenticatePlayer: returns user ID if success, -1 if incorrect password, -2 if incorrect username
DeregisterPlayer: returns true if success, false if failure
ChangePlayerPassword: returns true if success, false if failure
ChangePlayerUsername: returns true if success, false if failure
GetPlayerDetails: returns List of PlayerRecords if success, null if failure
RegisterPlayer: returns true if success, false if failure
UpdatePlayerDetails: returns true if success, false if failure

**********StoreItemsInterface

GetStoreItems: returns List of ItemRecords if success, null if failure

**********PlayerItemsInterface

BuyItem: returns true if success, false if failure
RemoveItem: returns true if success, false if failure
GetPlayerItems: returns List of ItemRecords if success, null if failure

**********PlayerScoreInterface
UpdateScore: returns true if success, false if failure
GetAllScores: returns List of ScoreRecords if success, null if failure
GetPlayerSCores: returns List of ScoreRecords if success, null if failure