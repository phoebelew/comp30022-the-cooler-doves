#Think twice before running this script...

USE `ProjectLadder`;

DELETE FROM `InventoryItem`;
DELETE FROM `StoreItem`;
DELETE FROM `ScoreLog`;
DELETE FROM `Message`;
DELETE FROM `UserAccount`;

INSERT INTO `UserAccount` (`UserName`, `Password`, `EmailAddress`, `FirstName`, `LastName`, `Currency`)
VALUES ("TestUser", "testpw", "test@test.com", "mr", "test", 50),
	   ("TestUser2", "password", "test2@test.com", "ms", "test", 100);
           
INSERT INTO `StoreItem` (`Name`, `Description`, `ImagePath`, `Price`)
VALUES ("TestItem", "This is a test item.", "testpath", 20);
    
INSERT INTO `InventoryItem` (`UserFK`, `ItemFK`)
VALUES ((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser"), (SELECT `ItemID` FROM `StoreItem` WHERE `Name` = "TestItem"));

INSERT INTO `ScoreLog` (`DateTime`, `ScoredPoints`, `UserFK`)
VALUES (NOW(), 20, (SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser"));

INSERT INTO `Message` (`SenderID`, `ReceiverID`, `Timestamp`, `SubjectLine`, `MessageContent`, `ResRead`, `SenderDeleted`, `ResDeleted`)
VALUES (((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser")), ((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser2")), NOW(), "Test message", "This is a test message content", FALSE, FALSE, FALSE),
       (((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser")), ((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser2")), NOW(), "Test message #2", "This is another test message content", FALSE, FALSE, FALSE),
       (((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser")), ((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser2")), NOW(), "Test message #3", "This is a third test message content", FALSE, FALSE, FALSE),
       (((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser")), ((SELECT `UserID` FROM `UserAccount` WHERE `UserName` = "TestUser2")), NOW(), "Test message #4", "This is the fourth and final test message content, i swear.", FALSE, FALSE, FALSE);
