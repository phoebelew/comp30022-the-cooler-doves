﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;

namespace ServerDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args[0].Equals("addcoins"))
            {
                Console.WriteLine("adding coins...");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://130.56.253.23/api/");
                    HttpResponseMessage response = client.GetAsync("users/" + args[1] + "/currency/add/" + args[2]).Result;
                    response.EnsureSuccessStatusCode();
                    string result = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Result: " + result);
                }
            }
            if(args[0].Equals("retrievecoins"))
            {
                Console.WriteLine("retrieving coins...");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://130.56.253.23/api/");
                    HttpResponseMessage response = client.GetAsync("users/currency/retrieve/" + args[1]).Result;
                    response.EnsureSuccessStatusCode();
                    string result = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Result: " + result);
                }
            }
            if (args[0].Equals("getallstoreitems"))
            {
                Console.WriteLine("Retrieving all store items...");
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://130.56.253.23/api/");
                    HttpResponseMessage response = client.GetAsync("store/items/getall").Result;
                    response.EnsureSuccessStatusCode();
                    string result = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Result: " + result);
                }
            }
            if (args[0].Equals("getitemimage"))
            {
                Console.WriteLine("Downloading image...");

                //string inputfilepath = @"C:\Temp\FileName.exe";
                string ftphost = "http://130.56.253.23:22/";
                //string ftpfilepath = args[1];

                //string ftpfullpath = "http://" + ftphost + ftpfilepath;

                using (var request = new WebClient())
                {
                    request.Credentials = new NetworkCredential("proj_admin", "coolerdoves");
                    request.DownloadFile(ftphost + args[1], "filename");
                    Console.WriteLine("image downloaded");
                }
            }

            if (args[0].Equals("buyitem"))
            {
                Console.WriteLine(string.Format("Buying item {0} for user {1}...", args[2], args[1]));
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://130.56.253.23/api/");
                    HttpResponseMessage response = client.GetAsync("users/" + args[1] + "/items/buy/" + args[2]).Result;
                    response.EnsureSuccessStatusCode();
                    string result = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Result: " + result);
                }
            }

            if (args[0].Equals("removeitem"))
            {
                Console.WriteLine(string.Format("Removing item type {0} for user {1}...", args[2], args[1]));
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://130.56.253.23/api/");
                    HttpResponseMessage response = client.GetAsync("users/" + args[1] + "/items/remove/" + args[2]).Result;
                    response.EnsureSuccessStatusCode();
                    string result = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Result: " + result);
                }
            }

            if (args[0].Equals("getplayeritems"))
            {
                Console.WriteLine(string.Format("Getting all items for user {0}", args[1]));
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://130.56.253.23/api/");
                    HttpResponseMessage response = client.GetAsync("users/items/get/" + args[1]).Result;
                    response.EnsureSuccessStatusCode();
                    string result = response.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Result: " + result);
                }
            }
        }
    }
}
