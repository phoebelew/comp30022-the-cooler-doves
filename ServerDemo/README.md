ServerDemo C# visual studio project
Authors: Kos Grillis (694699) and Phoebe Lew (829493)

Due to a number of unforeseen circumstances, integration of all server endpoints with the Unity front end could not be done. This project is a small console
application which is used to demo the server functionality that could not be integrated.