﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServerInterface
{
    static class PrivateMessageInterface
    {

        static HttpClient client = new HttpClient();

        private static void SetBaseAddress()
        {
            client.BaseAddress = new Uri("http://130.56.253.23/api/");
        }

        public static async Task<string> SendMessage(int senderid, int receiverid, string subject, string message)
        {
            SetBaseAddress();
            var values = new Dictionary<string, string>
            {
                {"from", senderid.ToString() },
                {"to", receiverid.ToString() },
                {"subject", subject },
                {"message", message }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("messages/sendmessage", content);
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public static async Task<string> CheckMessages(int userid)
        {
            SetBaseAddress();
            var response = await client.GetAsync(string.Format("users/{0}/checkmessages", userid));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public static async Task<string> MarkRead(int messageid)
        {
            SetBaseAddress();
            var response = await client.GetAsync(string.Format("messages/markread/{0}", messageid));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public static async Task<string> DeleteMessage(int userid, int messageid)
        {
            SetBaseAddress();
            var response = await client.GetAsync(string.Format("users/{0}/messages/delete/{1}", userid, messageid));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public static async Task<string> GetUnread(int userid)
        {
            //TODO: refactor to return an item record object rather than json string

            SetBaseAddress();
            var response = await client.GetAsync(string.Format("users/{0}/messages/get/unread", userid));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;

        }

        public static async Task<string> GetSent(int userid)
        {
            //TODO: refactor to return an item record object rather than json string

            SetBaseAddress();
            var response = await client.GetAsync(string.Format("users/{0}/messages/get/sent", userid));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;

        }

    }
}
