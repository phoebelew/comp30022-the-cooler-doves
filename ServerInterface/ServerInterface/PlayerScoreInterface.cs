﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ServerInterface
{
    static class PlayerScoreInterface
    {
        static HttpClient client = new HttpClient();

        private static void SetBaseAddress()
        {
            client.BaseAddress = new Uri("http://130.56.253.23/api/");
        }

        public static async Task<string> UpdateScore(int userid, int points)
        {
            SetBaseAddress();
            var response = await client.GetAsync(string.Format("scorebard/update/{0}/{1}/", userid, points));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public static async Task<Dictionary<string, int>> GetAllScores()
        {
            SetBaseAddress();
            var response = await client.GetAsync("scoreboard/getScores/");
            var responseString = await response.Content.ReadAsStringAsync();
            var rows = JsonConvert.DeserializeObject<Dictionary<string, int>>(responseString);
            return rows;

        }

        public static async Task<Dictionary<string, int>> GetPlayerScores(int userid)
        {
            SetBaseAddress();
            var response = await client.GetAsync("scoreboard/getScores/");
            var responseString = await response.Content.ReadAsStringAsync();
            var rows = JsonConvert.DeserializeObject<Dictionary<string, int>>(responseString);
            return rows;

        }
    }
}
