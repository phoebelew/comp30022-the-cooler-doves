﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace ServerInterface
{
    static class PlayerCurrencyInterface
    {
        static HttpClient client = new HttpClient();

        private static void setClientBaseAddress()
        {
            client.BaseAddress = new Uri("http://130.56.253.23/api/");
        }

        public static async Task<int> addCoins(String userID, String newCoins)
        {
            setClientBaseAddress();

            var response = await client.GetAsync(string.Format("users/{0}/currency/add/{1}", userID, newCoins));
            var responseString = await response.Content.ReadAsStringAsync();

            return Int32.Parse(responseString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static async Task<String> retrieveCoins(String userID)
        {
            setClientBaseAddress();

            var response = await client.GetAsync("users/currency/retrieve/" + userID);
            var resultString = await response.Content.ReadAsStringAsync();

            return resultString;
        }
    }
}
