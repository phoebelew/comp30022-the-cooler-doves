﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServerInterface
{
    static class StoreItemsInterface
    {
        static HttpClient client = new HttpClient();

        private static void SetBaseAddress()
        {
            client.BaseAddress = new Uri("http://130.56.253.23/api/");
        }

        public static async Task<string> GetStoreTtems()
        {
            //TODO: refactor to return an item record object rather than json string

            SetBaseAddress();
            var response = await client.GetAsync("store/items/getall");
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }
    }
}
