﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServerInterface
{
    static class PlayerItemsInterface
    {
        static HttpClient client = new HttpClient();

        private static void SetBaseAddress()
        {
            client.BaseAddress = new Uri("http://130.56.253.23/api/");
        }

        public static async Task<string> BuyItem(int userid, int itemid)
        {
            SetBaseAddress();
            var response = await client.GetAsync(string.Format("users/{0}/items/buy/{1}", userid, itemid));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;

        }

        public static async Task<string> RemoveItem(int userid, int itemid)
        {
            SetBaseAddress();
            var response = await client.GetAsync(string.Format("users/{0}/items/remove/{1}", userid, itemid));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public static async Task<string> GetPlayerItems(int userid)
        {
            //TODO: refactor to return an item record object rather than json string

            SetBaseAddress();
            var response = await client.GetAsync(string.Format("users/items/get/{0}", userid));
            var responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }
    }
}
