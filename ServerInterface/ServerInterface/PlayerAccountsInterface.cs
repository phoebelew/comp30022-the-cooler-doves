﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace ServerInterface
{
    static class PlayerAccountsInterface
    {
        static HttpClient client = new HttpClient();

        private static void setClientBaseAddress()
        {
            client.BaseAddress = new Uri("http://130.56.253.23/api/");
        }

        /// <summary>
        /// Authenticate a player. Can use to autheticate or log in.
        /// </summary>
        /// <param name="username">The username of the player, cannot be null</param>
        /// <param name="password">The password of the player, cannot be null</param>
        /// <returns>
        /// An integer userID (not less than zero) if the player is authenticated successfully
        /// -1 if the user is not authenticated due to incorrect password
        /// -2 if the user is not authenticated due to username not existant in database
        /// </returns>
        public static async Task<int> AuthenticatePlayer(String username, String password)
        {
            setClientBaseAddress();

            var values = new Dictionary<string, string>
            {
                {"UserName", username },
                { "Password", password }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("users/authenticate/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("ER_INCORRECT_USERNAME"))
            {
                return -2;
            }
            else if (responseString.Equals("FAILURE_AUTHENTICATED"))
            {
                return -1;
            }
            else
            {
                return Int32.Parse(responseString);
            }
        }

        /// <summary>
        /// Register a new player.
        /// </summary>
        /// <param name="username">players username, cannot be null</param>
        /// <param name="password">players password, cannot be null</param>
        /// <param name="emailAddress">players email address, if null send set as "null"</param>
        /// <param name="firstName">players first name, if null set as "null"</param>
        /// <param name="lastName">players last name, if null set as "null"</param>
        /// <returns>
        /// 1 if the player is successfully registered
        /// -1 if the player is not registered due to param username already taken
        /// 0 if massiven unknown error
        /// </returns>
        public static async Task<int> RegisterPlayer(String username, String password, String emailAddress, String firstName, String lastName)
        {
            setClientBaseAddress();

            var values = new Dictionary<string, string>
            {
                {"UserName", username },
                { "Password", password },
                {"FirstName", emailAddress },
                {"LastName", firstName },
                {"EmailAddress", lastName }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("users/add/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("ER_DUP_ENTRY"))
            {
                return -1;
            }
            else if (responseString.Equals("SUCCESS_REGISTRATION"))
            {
                return 1;
            }
            else
            {
                //massive unknown error
                return 0;
            }
        }

        /// <summary>
        /// Remove a players account.
        /// </summary>
        /// <param name="username">username of the player to be removed, cannot be null</param>
        /// <returns>
        /// 1 if the player is successfully removed
        /// 0 if massive unknown error
        /// </returns>
        public static async Task<int> DeregisterPlayer(String username)
        {
            setClientBaseAddress();

            var responseString = await client.GetStringAsync("users/remove/" + username);

            if (responseString.Equals("SUCCESS_DEREGISTRATION"))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Retrieve a players details from the database.
        /// </summary>
        /// <param name="userID">the userID to get details for</param>
        /// <returns>
        /// A string representation of a JSON object that contains the players details.
        /// </returns>
        public static async Task<String> RetrievePlayerDetails(int userID)
        {
            setClientBaseAddress();

            var response = await client.GetAsync("users/details/" + userID);
            var responseString = await response.Content.ReadAsStringAsync();

            return responseString;
        }

        /// <summary>
        /// Update a users basic information.
        /// 
        /// please note that all params emailAddress, firstName and lastName will be updated. If one of these
        /// should not be updated, please send through the original data.
        /// </summary>
        /// <param name="userID">userID of the player to update details for</param>
        /// <param name="password">password of the player to update details for</param>
        /// <param name="emailAddress">new email adress</param>
        /// <param name="firstName">new first name</param>
        /// <param name="lastName">new last name</param>
        /// <returns>
        /// 1 if the users details are successfully updated
        /// -1 if the users details are not updated due to provided username being non existant
        /// -2 if the users details are not updated due to incorrect password
        /// 0 if massive unknown error
        /// </returns>
        public static async Task<int> UpdatePlayerDetails(String userID, String password, String emailAddress, String firstName, String lastName)
        {
            setClientBaseAddress();

            var values = new Dictionary<string, string>
            {
                {"UserID", userID},
                {"Password", password },
                {"FirstName", firstName },
                {"LastName", lastName },
                {"EmailAddress", emailAddress }
            };
            
            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("users/update/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("SUCCESS_UPDATE"))
            {
                return 1;
            }
            else if (responseString.Equals("ER_INCORRECT_USERNAME"))
            {
                return -1;

            }
            else if (responseString.Equals("FAILURE_AUTHENTICATED"))
            {
                return -2;
            }
            else
            {
                //massive unknown error
                return 0;
            }
        }

        /// <summary>
        /// Update change a players username.
        /// </summary>
        /// <param name="userID">userID of the player to update username for</param>
        /// <param name="password">password of the player to update username for</param>
        /// <param name="newUsername">new username</param>
        /// <returns>
        /// 1 if the users details are successfully updated
        /// -1 if the users details are not updated due to provided username being non existant
        /// -2 if the users details are not updated due to incorrect password
        /// -3 if the users details are not updated because the new username has already been taken
        /// 0 if massive unknown error
        /// </returns>
        public static async Task<int> UpdatePlayerDetails(String userID, String password, String newUsername)
        {
            setClientBaseAddress();

            var values = new Dictionary<string, string>
            {
                {"UserID", userID },
                {"Password", password },
                {"NewUserName", newUsername }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("users/updateUsername/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("SUCCESS_USERNAME_UPDATE"))
            {
                return 1;
            }
            else if (responseString.Equals("ER_INCORRECT_USERNAME"))
            {
                return -1;

            }
            else if (responseString.Equals("FAILURE_AUTHENTICATED"))
            {
                return -2;
            }
            else if (responseString.Equals("ER_DUP_ENTRY"))
            {
                return -3;
            }
            else
            {
                //massive unknown error
                return 0;
            }
        }

        /// <summary>
        /// Update change a players password
        /// </summary>
        /// <param name="userID">ID of the user to change password for</param>
        /// <param name="password">current password of the user to change password for</param>
        /// <param name="newPassword">new password</param>
        /// <param name="notUsed">Used to differentiated between overloaded methods</param>
        /// <returns>
        /// 1 if the users details are successfully updated
        /// -1 if the users details are not updated due to provided username being non existant
        /// -2 if the users details are not updated due to incorrect password
        /// 0 if massive unknown error
        /// </returns>
        public static async Task<int> UpdatePlayerDetails(String userID, String password, String newPassword, bool notUsed)
        {
            setClientBaseAddress();

            var values = new Dictionary<string, string>
            {
                {"UserID", userID},
                {"Password", password },
                {"NewPassword", newPassword }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("users/updatePassword/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("SUCCESS_PASSWORD_UPDATE"))
            {
                return 1;
            }
            else if (responseString.Equals("ER_INCORRECT_USERNAME"))
            {
                return -1;

            }
            else if (responseString.Equals("FAILURE_AUTHENTICATED"))
            {
                return -2;
            }
            else
            {
                //massive unknown error
                return 0;
            }
        }
    }
}
