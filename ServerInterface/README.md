ServerInterface C# Visual Studio project  
Authors: Kos Grillis (694699) and Phoebe Lew (829493)

This is an older version of the server interface project. It evolved into ServerLibrary which was used in the final project. Unfortunately this code had to be
abandoned as it uses async methods which are incompatible with Unity. However the project was kept as it contains good code which is kept for reference, and was
also used to test the functionality of the server.