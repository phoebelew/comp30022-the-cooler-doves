﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ProjectLadderApiTest
{
    class UpdateProfileTest : ITest
    {
       
        public UpdateProfileTest(HttpClient c): base(c) {}

        public override async Task RunTest()
        {
            Console.WriteLine("Running update profile tests...");
            await RetrieveDetailsTest();
            await UpdateBasicTest();
            await UpdatePasswordTest();
            await UpdateUsernameTest();
            Console.WriteLine();
 
        }

        private async Task RetrieveDetailsTest()
        {
            var response = await c.GetAsync("users/details/19/");
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString != "FAILURE_RETRIEVE_DETAILS")
            {
                Console.WriteLine("[RetrieveDetailsTest          PASSED]");
            }
            else
            {
                Console.WriteLine("[RetrieveDetailsTest          FAILED]");
            }

        }
        private async Task UpdateBasicTest()
        {
            var values = new Dictionary<string, string>
            {
                {"UserID", "35"},
                {"Password", "fakepassword" },
                {"FirstName", "name" },
                {"LastName", "lastname"},
                {"EmailAddress", "test@test.com"}
            };
            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("users/update/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString == "SUCCESS_UPDATE")
            {
                Console.WriteLine("[UpdateBasicTest              PASSED]");
            }
            else
            {
                Console.WriteLine("[UpdateBasicTest              FAILED]");
                Console.WriteLine(responseString);
                Console.WriteLine();
                
            }

        }
        private async Task UpdatePasswordTest()
        {
            var values = new Dictionary<string, string>
            {
                {"UserID", "20"},
                {"Password", "password" },
                {"NewPassword", "password" }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("users/updatePassword/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString == "SUCCESS_PASSWORD_UPDATE")
            {
                Console.WriteLine("[UpdatePasswordTest           PASSED]");
            }
            else
            {
                Console.WriteLine("[UpdatePasswordTest           FAILED]");
                Console.WriteLine(responseString);
                Console.WriteLine();
            }

        }
        private async Task UpdateUsernameTest()
        {
            var values = new Dictionary<string, string>
            {
                {"UserID", "20"},
                {"Password", "password" },
                {"NewUserName", "testUN" }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("users/updateUsername/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString == "SUCCESS_USERNAME_UPDATE")
            {
                Console.WriteLine("[UpdateUsernameTest           PASSED]");
            }
            else
            {
                Console.WriteLine("[UpdateUsernameTest           FAILED]");
                Console.WriteLine(responseString);
                Console.WriteLine();
            }

        }


    }
}
