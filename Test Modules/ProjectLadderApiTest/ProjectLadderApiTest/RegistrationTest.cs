﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace ProjectLadderApiTest
{
    class RegistrationTest : ITest
    {

        public RegistrationTest(HttpClient c): base(c){}

        public override async Task RunTest()
        {
            Console.WriteLine("Running registration tests...");

            await RegistrationFailureTest();
            await RegistrationSuccessTest();

        }

        private async Task RegistrationFailureTest()
        {
            // Registration failure test --> should fail due to duplicate entry
            var values = new Dictionary<string, string>
            {
                {"UserName", "fakeuser" },
                { "Password", "fakepassword"},
                {"FirstName", "null" },
                {"LastName", "null" },
                {"EmailAddress", "null" }
            };
            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("users/add/", content);
            var responseString = await response.Content.ReadAsStringAsync();
            if (responseString != "SUCCESS_REGISTRATION")
            {
                Console.WriteLine("[RegistrationFailureTest      PASSED]");
            }
            else
            {
                Console.WriteLine("[RegistrationFailureTest      FAILED]");
                Console.WriteLine(responseString);
            }

        }

        private async Task RegistrationSuccessTest()
        {
            // Registration success test --> should succeed
            var values = new Dictionary<string, string>
            {
                {"UserName", "fakeuser2" },
                { "Password", "fakepassword"},
                {"FirstName", "first" },
                {"LastName", "last" },
                {"EmailAddress", "email" }
            };
            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("users/add/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("SUCCESS_REGISTRATION"))
            {
                Console.WriteLine("[RegistrationSuccessTest      PASSED]");
            }
            else
            {
                Console.WriteLine("[RegistrationSuccessTest      FAILED]");
                Console.WriteLine(responseString);
            }

        }

    }
}
