﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLadderApiTest
{
    class CurrencyTest : ITest
    {
       
        public CurrencyTest(HttpClient c): base(c){}


        //ENSURE TO UPDATE THE DATABASE SO TESTUSER ALWAYS HAS 50 COINS BEFORE RUNNING THE TEST
        public override async Task RunTest()
        {
            Console.WriteLine("Running currency test...");

            string strExpected = "50";
            var intExpected = Int32.Parse(strExpected);

            // get the user's current coins
            var response = await c.GetAsync("users/currency/retrieve/19");
            var resultOne = await response.Content.ReadAsStringAsync();
            if (resultOne == strExpected)
            {
                // add 50 to the user's coins
                response = await c.GetAsync(string.Format("users/19/currency/add/{0}", strExpected));
                var addResult = await response.Content.ReadAsStringAsync();

                if (addResult == "ER_INVALID_VALUES")
                {
                    Console.WriteLine("[CurrencyTest                 FAILED]");
                    Console.WriteLine(addResult);
                    return;
                }

                // get the user's coins again...should be 100
                response = await c.GetAsync("users/currency/retrieve/19");
                var resultTwo = await response.Content.ReadAsStringAsync();

                if (Int32.Parse(resultTwo) == intExpected * 2)
                {
                    Console.WriteLine("[CurrencyTest                 PASSED]");
                }
                else
                {
                    Console.WriteLine("[CurrencyTest                 FAILED]");
                    Console.WriteLine(Int32.Parse(resultTwo));
                    Console.WriteLine();

                }
            }
            else
            {
                Console.WriteLine("[CurrencyTest                 FAILED]");
                Console.WriteLine(resultOne);
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
