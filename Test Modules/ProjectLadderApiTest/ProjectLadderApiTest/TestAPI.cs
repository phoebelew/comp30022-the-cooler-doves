﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;

namespace ProjectLadderApiTest
{
    /// Class which will run test cases against all functionality of the RESTful API.
    class TestAPI
    {
        private static AuthenticateTest at;
        private static RegistrationTest rt;
        private static DeregistrationTest dt;
        private static UpdateProfileTest up;
        private static CurrencyTest ct;
        private static MessageTest mt;

        /// Run all defined tests to the API.

        static void Main(string[] args)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://130.56.253.23/api/");

            //Create all required tests...
            at = new AuthenticateTest(client);
            rt = new RegistrationTest(client);
            up = new UpdateProfileTest(client);
            dt = new DeregistrationTest(client);

            ct = new CurrencyTest(client);
            mt = new MessageTest(client);

            RunTests();

            //Pause...
            Console.Read();
        }

        /// Runs an integration test on a user - registration, authentication and deregistration.
        /// This test will create a new user, authenticate it and then deregister it.
   
        private static async void RunTests()
        {
            try
            {
                await rt.RunTest();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e.InnerException.Message);
            }
           
            Console.WriteLine();
            await at.RunTest();
            Console.WriteLine();
            await up.RunTest();
            Console.WriteLine();
            await dt.RunTest();
            Console.WriteLine();


            await ct.RunTest();
            Console.WriteLine();
            await mt.RunTest();

        }

    }
}
