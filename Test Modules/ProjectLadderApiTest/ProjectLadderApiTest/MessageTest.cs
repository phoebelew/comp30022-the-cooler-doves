﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectLadderApiTest
{
    class MessageTest : ITest
    {
        public MessageTest(HttpClient c) : base(c){}

        public override async Task RunTest()
        {
            Console.WriteLine("Running message tests...");
            await SendMessageTest();
            Console.WriteLine();
        }

        public async Task SendMessageTest()
        {
            var values = new Dictionary<string,string>
            {
                {"from", "19" },
                {"to", "20" },
                {"subject", "test message subject" },
                {"message", "this is the body of a test message" }
            };

            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("messages/sendmessage", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("SUCCESS_SEND_MESSAGE"))
            {
                Console.WriteLine("[SendMessageTest              PASSED]");
            }
            else
            {
                Console.WriteLine("[SendMessageTest              FAILED]");
                Console.WriteLine(responseString);
                Console.WriteLine();
            }
        }
    }
}
