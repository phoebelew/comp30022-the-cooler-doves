﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLadderApiTest
{
    class DeregistrationTest: ITest
    {

        public DeregistrationTest(HttpClient c): base(c){}

        public override async Task RunTest()
        {
            Console.WriteLine("Running deregistration test...");
            var response = await c.GetStringAsync("users/remove/36");

            if (response == "SUCCESS_DEREGISTRATION")
            {
                Console.WriteLine("[DeregisterTest               PASSED]");
            }
            else
            {
                Console.WriteLine("[DeregisterTest               FAILED]");
                Console.WriteLine(response);
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
