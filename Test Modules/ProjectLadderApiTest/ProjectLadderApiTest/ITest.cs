﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ProjectLadderApiTest
{
    public abstract class ITest
    {
        protected HttpClient c;

        protected ITest(HttpClient c)
        {
            this.c = c;
        }

        public abstract Task RunTest();

    }
}
