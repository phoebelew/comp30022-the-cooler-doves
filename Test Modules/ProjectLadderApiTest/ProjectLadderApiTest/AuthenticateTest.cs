﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Runtime.InteropServices;


namespace ProjectLadderApiTest
{
    /// Tests that API user authentication is correct, using a series of unit tests.
    public class AuthenticateTest : ITest
    {

        public AuthenticateTest(HttpClient c) : base(c) {}

        /// Runs 3 unit test calls to the API authentication route, to test all functionality.
        public override async Task RunTest()
        {
            Console.WriteLine("Running authenticate tests...");
           await AuthenticateFailureTest1();
           await AuthenticateFailureTest2();
           await AuthenticateSuccessTest();
            Console.WriteLine();

        }

        //Unit test to ensure that in incorrect username parameter is returned as such...
        private async Task AuthenticateFailureTest1()
        {

            var values = new Dictionary<string, string>
            {
                {"UserName", "aa;ejfajeijd" },
                { "Password", "jaweijcnknd"}
            };
            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("users/authenticate/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("ER_INCORRECT_USERNAME"))
            {
                Console.WriteLine("[AuthenticateSuccessTest      PASSED]");
            }
            else
            {
                Console.WriteLine("[AuthenticateSuccessTest      FAILED]");
                Console.WriteLine(responseString);
                Console.WriteLine();
            }

        }

        //Unit test to ensure that an incorrect password results in a failed authentication...
        private async Task AuthenticateFailureTest2()
        {
            var values = new Dictionary<string, string>
            {
                { "UserName", "fakeuser2" },
                { "Password", "fakepassword234534"}
            };
            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("users/authenticate/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("FAILURE_AUTHENTICATED"))
            {
                Console.WriteLine("[AuthenticateSuccessTest      PASSED]");
            }
            else
            {
                Console.WriteLine("[AuthenticateSuccessTest      FAILED]");
                Console.WriteLine(responseString);
                Console.WriteLine();
            }

        }

        //Unit test to ensure that a correct username and password results in a successful authentication...
        private async Task AuthenticateSuccessTest()
        {
            var values = new Dictionary<string, string>
            {
                {"UserName", "fakeuser" },
                { "Password", "fakepassword"}
            };
            var content = new FormUrlEncodedContent(values);
            var response = await c.PostAsync("users/authenticate/", content);
            var responseString = await response.Content.ReadAsStringAsync();

            if (responseString.Equals("ER_INCORRECT_USERNAME") || responseString.Equals("FAILURE_AUTHENTICATED"))
            {
                Console.WriteLine("[AuthenticateSuccessTest      FAILED]");
            }
            else
            {
                Console.WriteLine("[AuthenticateSuccessTest      PASSED]");
            }
        }
    }
}
