/* Server-test.js
 *
 * Contains a modified, shortened version of server.js for use in the demo.
 * Endpoints used were not implemented on the Unity side, so this file is used
 * to demonstrate that the backend functionality was completed. The code is
 * identical to the server.js endpoints, but with added logging for the demo.
 *
 * Authors: Phoebe Lew <plew@student.unimelb.edu.au><829493> and Kos Grillis <cgrillis@student.unimelb.edu><694699>
 *
 * For COMP30022 IT Project, semester 2, 2016
 */

var express = require("express");
var mysql = require('mysql');
var bodyParser = require('body-parser');

//If you don't need to use promises with mysql, use the following...
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'proj_admin',
    password: 'coolerdoves',
    database: 'ProjectLadder'
});

//If you need to use promises with mysql, use the following....
var mysqlPromise = require('promise-mysql');
var promiseConnection;
mysqlPromise.createConnection({
    host: 'localhost',
    user: 'proj_admin',
    password: 'coolerdoves',
    database: 'ProjectLadder'
}).then(function(conn){
    promiseConnection = conn;
});

var app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
}));

// TODO: Refactor to use pooled connections
connection.connect(function(err) {
    if (!err) {
        console.log("Database is connected ...");
    } else {
        console.log("Error connecting database");
    }
});

// **************************************** PLAYER CURRENCY *************************************************

/*
Functionality to retrieve the players current number of game coins.

DO NOT provide with null values. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to pass authenticated username.

Params:
    UserID - the ID of the user to retrieve details for. Should not be null.

Returns:
    A string representation of the number of game coins a user has. Please ensure
    to parse this correctly on the client side.
*/
app.get("/api/users/currency/retrieve/:UserID", function(req, res){

    var userID = req.params.UserID;
    console.log("Retrieving items for user " + userID + "...");

    connection.query('SELECT Currency FROM UserAccount WHERE UserID = ?', userID, function(err, resp){
        if (!err){
            var string = JSON.stringify(resp);
            var jsonResp = JSON.parse(string);
            res.send((jsonResp[0].Currency).toString());
        }
        else {
            res.send(err);
        }
    });

    connection.end;
});

/*
Functionality to increase a players current number of game coins.

DO NOT provide with null values. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to pass authenticated username.

Params:
    UserID - the id of the user to add coins to
    NewCoins - an integer number of coins that will be added to the players current coins.

Returns:
    ER_INVALID_VALUES if the value of the coins is negative
    SUCCESS_UPDATE if no errors and coins are successfully added
*/
app.get("/api/users/:UserID/currency/add/:NewCoins", function(req, res){

    var newcoins;
    var addedcoins;
    var addedCoins = parseInt(req.params.NewCoins);

    console.log("Checking that the number of coins is positive...");
    if (addedCoins < 0){
        //someone is trying to decrement tsk tsk...
        res.send("ER_INVALID_VALUES");
    } else {
        console.log("Updating number of coins for user " + req.params.UserID + "...");

        var vals = [addedCoins, req.params.UserID];
        connection.query('UPDATE UserAccount SET Currency = Currency + ? WHERE UserID = ?', vals, function(err, resp){
            if(err){
                res.send(err);

            } else {
                res.send("SUCCESS_ADD");
            }
        });
    }
    connection.end;
});

// ****************************************** STORE ENDPOINTS *************************************************

/*
Function to get all store items.

Returns:
    A JSON array of all items in the following format:

    [
        {
            "ItemID" : Integer,
            "Name" : String,
            "Description" : String,
            "ImagePath" : String,
            "Price" : Integer
        },
        ...
    ]
*/
app.get("/api/store/items/getall", function (req, res) {
    console.log("Getting all items from the store...");
    connection.query('SELECT * FROM StoreItem', function(err, rows) {
        if (err) {
            res.send(err);
        } else {
            var jsonRows = JSON.stringify(rows);
            var parsedRows = JSON.parse(jsonRows);
            res.send(parsedRows);
        }
    });
});

// ****************************************** PLAYER ITEMS    *************************************************

/*
Functionality to add a store bought item to a users account.

Does three things:
    1. Checks that the user has enough coins to buy the item
    2. If enough, subtracts the cost of the item from the users coins
    3. Adds the item to the users inventory table (UserInventory)

DO NOT provide with null values. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to pass authenticated user ID.

Params:
    UserID - The ID of the user that is buying an item. Should not be null.
    ItemID - The ID of the item that the user wants to buy. Should not be null.

Returns:
    ER_INVALID_ITEM if the wrong item ID is somehow passed into the function
    ER_NOT_ENOUGH_COINS if the player does not have enough coins to buy this item
    SUCCESS_BUY_ITEM if the item is successfully added to the players inventory
*/
app.get("/api/users/:UserID/items/buy/:ItemID", function(req, res){

    var itemPrice, userCoins, userID, itemID;

    userID = parseInt(req.params.UserID);
    itemID = parseInt(req.params.ItemID);
    var queryString = "SELECT Price FROM StoreItem WHERE ItemID = ?";

    console.log("Getting the price of itemID " + itemID + "...");
    promiseConnection.query(queryString, itemID).then(function(resp){
        var resp_ = resp[0];
        return resp_;
    }).then(function(resp_){

        var itemPrice = parseInt(resp_.Price);
        var queryString2 = "SELECT Currency FROM UserAccount WHERE UserID = ?";

        promiseConnection.query(queryString2, userID).then(function(resp2){
            var resp2_ = resp2[0];
            return resp2_;
        }).then(function(resp2_){
            var userCoins = parseInt(resp2_.Currency);
        }).then(function(itemprice, usercurrency){
            //console.log(itemPrice);
            if (itemPrice > userCoins){
                console.log("Error: player doesn't have enough coins");
                res.send("ER_NOT_ENOUGH_COINS");
            } else {
                connection.query('UPDATE UserAccount SET Currency = Currency - ? WHERE UserID = ?', [itemPrice, userID], function(err, resp){
                    //massive error...
                    if (err){
                        res.send(err);
                    }
                });
                console.log("Add item into user inventory...");
                connection.query('INSERT INTO InventoryItem (UserFK, ItemFK) VALUES (?, ?)', [userID, itemID] , function(err, resp){
                    if (!err){
                        res.send("SUCCESS_BUY_ITEM");
                    }
                });
            }
        });
    })
});

/*
Functionality to remove an item from a players inventory. This should be called
when the player uses an item in game.

DO NOT provide with null values. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to pass authenticated user ID.

Params:
    UserID - The ID of the user that should have the item removed from their inventory. Should not be null.
    ItemID - The ID of the item that should be removed from the above users inventory. Should not be null.

Returns:
    SUCCESS_ITEM_DELETED if the item is successfully deleted from the players inventory
*/
app.get("/api/users/:UserID/items/remove/:ItemID", function(req, res){

    var vals;

    vals = [parseInt(req.params.UserID), parseInt(req.params.ItemID)];
    console.log("Removing itemID " + req.params.ItemID + " from UserID " + req.params.UserID + " inventory...");
    connection.query('DELETE FROM InventoryItem WHERE UserFK = ? AND ItemFK = ? LIMIT 1', vals, function(err, resp){
        if (!err){
            res.send("SUCCESS_ITEM_DELETED");
        } else {
            res.send(err);
        }
    });
    connection.end;
});

/*
Function to get all of a player's items
*/
app.get("/api/users/items/get/:UserID", function (req, res) {

    console.log("Getting all items for userID " + req.params.UserID + "...");

    var queryString = `SELECT StoreItem.Name, StoreItem.Description, StoreItem.ImagePath, COUNT(*)
                       FROM InventoryItem
                       JOIN StoreItem ON InventoryItem.ItemFK = StoreItem.ItemID
                       WHERE InventoryItem.UserFK = ?
                       GROUP BY StoreItem.Name`;
    
    var vals = [req.params.userID];
    connection.query(queryString, vals, function(err, rows) {
        if (err) {
            res.send(err);
        } else {
        
            var jsonRows = JSON.stringify(rows);
            var parsedRows = JSON.parse(jsonRows);
            res.send(jsonRows);
        }
    });
});

app.listen(80, function() {
    console.log('Server listening on port 80');
});