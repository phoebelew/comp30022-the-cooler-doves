ServerLibrary C# Visual Studio project  
Authors: Phoebe Lew (829493) and Kos Grillis (694699)

This project is the evolved version of the ServerInterface visual studio project. It uses Unity-supported libraries and Unity-friendly coding so as to 
make intergration with Unity a trivial task for the front end team. 

The project contains a number of functions which may be called by the front-end team (briefly defined below). These all call their own custom handlers 
so that the server data is parsed into a friendly format before being returned. Hence the Unity front end team WILL NOT need to do any parsing of server 
data. 

FUNCTIONALITY OF ServerLibrary functions defined below to make things easy for the Unity team:
----------------------------------------------------------------------------------------------

**********PlayerCurrencyInterface
AddCoins: returns true if success, false if failure
RetrieveCoins: returns # of coins if success, -1 if failure

**********PlayerScoreInterface
AuthenticatePlayer: returns user ID if success, -1 if incorrect password, -2 if incorrect username
DeregisterPlayer: returns true if success, false if failure
ChangePlayerPassword: returns true if success, false if failure
ChangePlayerUsername: returns true if success, false if failure
GetPlayerDetails: returns List of PlayerRecords if success, null if failure
RegisterPlayer: returns true if success, false if failure
UpdatePlayerDetails: returns true if success, false if failure

**********StoreItemsInterface
GetStoreItems: returns List of ItemRecords if success, null if failure

**********PlayerItemsInterface
BuyItem: returns true if success, false if failure
RemoveItem: returns true if success, false if failure
GetPlayerItems: returns List of ItemRecords if success, null if failure

**********PlayerScoreInterface
UpdateScore: returns true if success, false if failure
GetAllScores: returns List of ScoreRecords if success, null if failure
GetPlayerSCores: returns List of ScoreRecords if success, null if failure