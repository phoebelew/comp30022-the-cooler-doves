﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using UnityEngine;


namespace ServerLibrary
{
    public class RecordTypes : MonoBehaviour
    {
        [Serializable]
        public class ScoreRecord
        {
            public string username { get; set; }
            public int score { get; set; }
        }

        [Serializable]
        public class ScoreList
        {
            public List<ScoreRecord> scores;
        }

        [Serializable]
        public class PlayerRecord
        {
            public string username { get; set; }
            public string emailAddress { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string currency { get; set; }
        }

        [Serializable]
        public class ItemRecord
        {
            public string name  { get; set; }
            public string description { get; set; }
            public string image_path { get; set; }
            public int count { get; set; }
        }
    }
}
