﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using UnityEngine;
using UnityEngine.Networking.Match;


namespace ServerLibrary
{
    static class InterfaceHandlers
    {
        // PlayerCurrencyInterface Handlers

        public static bool AddCoinsHandler(string response)
        {
            return (response == "SUCCESS_ADD");
        }

        public static int RetrieveCoinsHandler(string response)
        {
            return Int32.Parse(response);
        }

        // PlayerAccountInterface Handlers

        public static int AuthenticatePlayerHandler(string responseString)
        {
            if (responseString.Equals("ER_INCORRECT_USERNAME"))
            {
                return -2;
            }
            else if (responseString.Equals("FAILURE_AUTHENTICATED"))
            {
                return -1;
            }
            else
            {
                return Int32.Parse(responseString);
            }
        }

        public static bool DeregisterPlayerHandler(string reponse)
        {
            return (reponse == "SUCCESS_DEGISTRATION");
        }

        public static bool ChangePlayerPasswordHandler(string response)
        {
            return (response == "SUCCESS_PASSWORD_UPDATE");
        }

        public static bool ChangePlayerUsernameHandler(string response)
        {
            return (response == "SUCCESS_USERNAME_UPDATE");
        }

        public static List<RecordTypes.PlayerRecord> GetPlayerDetailsHandler(string response)
        {

            return response == "error" ? null : JsonUtility.FromJson<List<RecordTypes.PlayerRecord>>(response);

        }

        public static bool RegisterPlayerHandler(string responseString)
        {
            return (responseString == "SUCCESS_REGISTRATION");
        }

        public static bool UpdatePlayerDetailsHandler(string response)
        {
            return (response == "SUCCESS_UPDATE");
        }

        // StoreItemsInterface Handlers

        public static List<RecordTypes.ItemRecord> GetStoreItemsHandler(string response)
        {
            return response == "error" ? null : JsonUtility.FromJson<List<RecordTypes.ItemRecord>>(response);
        }

        // PlayerItemsInterface Handlers

        public static bool BuyItemHandler(string response)
        {
            return (response == "SUCCESS_BUY_ITEM");
        }

        public static bool RemoveItemHandler(string response)
        {
            return (response == "SUCCESS_ITEM_DELETED");
        }

        public static List<RecordTypes.ItemRecord> GetPlayerItemsHandler(string response)
        {
            return response == "error" ? null : JsonUtility.FromJson<List<RecordTypes.ItemRecord>>(response);
        }

        // PlayerScoreInterface Helpers
        public static bool UpdateScoreHandler(string response)
        {
            return (response == "SUCCESS_UPDATE_SCORE");
        }

        public static RecordTypes.ScoreList GetAllScoresHandler(string response)
        {
            return JsonUtility.FromJson<RecordTypes.ScoreList>(response);
        }

        public static List<RecordTypes.ScoreRecord> GetPlayerScoresHandler(string response)
        {
            return JsonUtility.FromJson<List<RecordTypes.ScoreRecord>>(response);
        }
    }
}
