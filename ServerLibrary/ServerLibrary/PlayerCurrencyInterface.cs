﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace ServerLibrary
{
    class PlayerCurrencyInterface : MonoBehaviour
    {
        private string baseAddress = "http://130.56.253.23/api/";

        public void AddCoins()
        {
            // TODO: pass in userid and # of coins
            var userid = 19;
            var coins = 20;
            StartCoroutine(RunAddCoins(userid, coins));
        }

        private IEnumerator RunAddCoins(int userid, int coins)
        {
            var www = UnityWebRequest.Get(string.Format("{0}users/{1}/currency/add/{2}", baseAddress, userid, coins));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.AddCoinsHandler(www.downloadHandler.text);
            }
        }

        public void RetrieveCoins()
        {
            // TODO: pass in uid
            var userid = 19;
            StartCoroutine(RunRetrieveCoins(userid));
        }

        private IEnumerator RunRetrieveCoins(int userid)
        {
            var www = UnityWebRequest.Get(string.Format("{0}users/currency/retrieve/{1}", baseAddress, userid));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.RetrieveCoinsHandler(www.downloadHandler.text);
            }
        }
    }
}
