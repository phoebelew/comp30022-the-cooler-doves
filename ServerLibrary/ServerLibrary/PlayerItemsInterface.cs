﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace ServerLibrary
{
    class PlayerItemsInterface : MonoBehaviour
    {
        private string baseAddress = "http://130.56.253.23/api/";

        public void BuyItem()
        {
            // TODO: userid, itemid
            var userid = 19;
            var itemid = 2;
            StartCoroutine(RunBuyItem(userid, itemid));
        }

        private IEnumerator RunBuyItem(int userid, int itemid)
        {
            var www = UnityWebRequest.Get(string.Format("{0}users/{1}/items/buy/{2}", baseAddress, userid, itemid));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.BuyItemHandler(www.downloadHandler.text);
            }
        }

        public void RemoveItem()
        {
            // TODO: userid, itemid
            var userid = 19;
            var itemid = 2;
            StartCoroutine(RunRemoveItem(userid, itemid));
        }

        private IEnumerator RunRemoveItem(int userid, int itemid)
        {
            var www = UnityWebRequest.Get(string.Format("{0}users/{1}/items/remove/{2}", baseAddress, userid, itemid));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.RemoveItemHandler(www.downloadHandler.text);
            }
        }

        public void GetPlayerItems()
        {
            // TODO: userid
            var userid = 19;
            StartCoroutine(RunGetPlayerItems(userid));
        }

        private IEnumerator RunGetPlayerItems(int userid)
        {
            var www = UnityWebRequest.Get(string.Format("{0}users/items/get/{1}", baseAddress, userid));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.GetPlayerItemsHandler(www.downloadHandler.text);
            }
        }
    }
}
