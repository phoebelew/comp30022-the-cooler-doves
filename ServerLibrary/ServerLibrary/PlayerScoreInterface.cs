﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;


namespace ServerLibrary
{
    class PlayerScoreInterface : MonoBehaviour
    {
        private string baseAddress = "http://130.56.253.23/api/";

        public void UpdateScore()
        {
            // TODO: pass in userID from user object
            var userid = 19;
            var points = 253;
            StartCoroutine(RunUpdateScore(userid, points));
        }

        private IEnumerator RunUpdateScore(int userid, int points)
        {
            var www = UnityWebRequest.Get(string.Format("{0}scoreboard/update/{1}/{2}/", baseAddress, userid, points));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.UpdateScoreHandler(www.downloadHandler.text);
            }
        }

        public void GetAllScores()
        {
            StartCoroutine(RunGetAllScores());
        }

        private IEnumerator RunGetAllScores()
        {
            var www = UnityWebRequest.Get(string.Format("{0}scoreboard/getScores/", baseAddress));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string jsonString = "{\"scores\":" + www.downloadHandler.text + "}";
                InterfaceHandlers.GetAllScoresHandler(jsonString);
            }
        }

        public void GetPlayerScores()
        {
            // TODO: Get user id
            var userid = 19;
            StartCoroutine(RunGetPlayerScores(userid));
        }

        private IEnumerator RunGetPlayerScores(int userid)
        {
            var www = UnityWebRequest.Get(string.Format("{0}users/scoreboard/get/{1}/", baseAddress, userid));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                //                Debug.Log();
                InterfaceHandlers.GetPlayerScoresHandler(www.downloadHandler.text);
            }
        }

        

    
    }

}
