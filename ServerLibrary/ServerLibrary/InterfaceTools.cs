﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerLibrary
{
    static class InterfaceTools
    {
        public static String GetAccountsUrl(String substring)
        {
            return GetBaseURL() + "users/" + substring;
        }

        private static String GetBaseURL()
        {
            return "http://130.56.253.23/api/";
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            return bytes;
        }
    }
}

