﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace ServerLibrary
{
    public class PlayerAccountsInterface : MonoBehaviour
    {
        private string baseAddress = "http://130.56.253.23/api/";

        public InputField usernameInput;
        public InputField passwordInput;

        public void AuthenticatePlayer()
        {
            StartCoroutine(AuthPlayer("TestUser", "testpw"));
        }

        public void RegisterPlayer()
        {
            StartCoroutine(RegPlayer("test", "test", "test", "test", "test"));
        }

        public void GetPlayerDetails()
        {
            StartCoroutine(GetPlayerDeets("19"));
        }

        public void DeregisterPlayer()
        {
            var playerToBeDeregistered = "";
            StartCoroutine(DeregPlayer(playerToBeDeregistered));
        }

        public void ChangePlayerPassword()
        {
            var placeholder1 = "";
            var placeholder2 = "";
            var placeholder3 = "";
            StartCoroutine(ChangePassword(placeholder1, placeholder2, placeholder3));
        }

        public void ChangePlayerUsername()
        {
            var placeholder1 = "";
            var placeholder2 = "";
            var placeholder3 = "";
            StartCoroutine(ChangeUsername(placeholder1, placeholder2, placeholder3));
        }

        public void UpdatePlayerDetails()
        {
            var userid;
            var firstname;
            var lastname;
            var email;
            var password;
            StartCoroutine(RunUpdatePlayerDetails(userid, firstname, lastname, email, password));
        }

        private IEnumerator RunUpdatePlayerDetails(int userid, string firstname, string lastname, string email, string password)
        {
            var form = new WWWForm();
            form.AddField("UserID", userid);
            form.AddField("Password", password);
            form.AddField("FirstName", firstname);
            form.AddField("LastName", lastname);
            form.AddField("EmailAddress", password);

            var request = UnityWebRequest.Post(string.Format("{0}/users/update/", baseAddress), form);
            yield return request.Send();

            if (request.isError)
            {
                Debug.Log(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                InterfaceHandlers.UpdatePlayerDetailsHandler(request.downloadHandler.text);
            }
        }

        private IEnumerator AuthPlayer(String username, String password)
        {
            var form = new WWWForm();
            form.AddField("Username", username);
            form.AddField("Password", password);

            var request = UnityWebRequest.Post(InterfaceTools.GetAccountsUrl("authenticate"), form);
            yield return request.Send();

            if (request.isError)
            {
                Debug.Log(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                InterfaceHandlers.AuthenticatePlayerHandler(request.downloadHandler.text);
            }
        }   

        private IEnumerator RegPlayer(String username, String password, String emailAddress, String firstName, String lastName)
        {
            var form = new WWWForm();
            form.AddField("Username", username);
            form.AddField("Password", password);
            form.AddField("EmailAddress", emailAddress);
            form.AddField("FirstName", firstName);
            form.AddField("LastName", lastName);

            var request = UnityWebRequest.Post(InterfaceTools.GetAccountsUrl("add"), form);
            yield return request.Send();

            if (request.isError)
            {
                Debug.Log(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                InterfaceHandlers.RegisterPlayerHandler(request.downloadHandler.text);
            }
        }

        private IEnumerator GetPlayerDeets(String userid)
        {
            var www = UnityWebRequest.Get(InterfaceTools.GetAccountsUrl("details") + "/" + userid);
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.GetPlayerDetailsHandler(www.downloadHandler.text);
            }
        }

        private IEnumerator DeregPlayer(String userid)
        {
            var www = UnityWebRequest.Get(InterfaceTools.GetAccountsUrl("remove") + "/" + userid);
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.DeregisterPlayerHandler(www.downloadHandler.text);
            }
        }

        private IEnumerator ChangePassword(String userid, String oldPassword, String newPassword)
        {
            var form = new WWWForm();
            form.AddField("UserID", userid);
            form.AddField("Password", oldPassword);
            form.AddField("NewPassword", newPassword);

            var request = UnityWebRequest.Post(InterfaceTools.GetAccountsUrl("updatePassword"), form);
            yield return request.Send();

            if (request.isError)
            {
                Debug.Log(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                InterfaceHandlers.ChangePlayerPasswordHandler(request.downloadHandler.text);
            }
        }

        private IEnumerator ChangeUsername(String userid, String password, String newUsername)
        {
            var form = new WWWForm();
            form.AddField("UserID", userid);
            form.AddField("Password", password);
            form.AddField("NewUserName", newUsername);

            var request = UnityWebRequest.Post(InterfaceTools.GetAccountsUrl("updateUsername"), form);
            yield return request.Send();

            if (request.isError)
            {
                Debug.Log(request.error);
            }
            else
            {
                Debug.Log(request.downloadHandler.text);
                InterfaceHandlers.ChangePlayerUsernameHandler(request.downloadHandler.text);
            }
        }
    }
}
