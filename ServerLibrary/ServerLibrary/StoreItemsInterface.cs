﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace ServerLibrary
{
    class StoreItemsInterface : MonoBehaviour
    {
        private string baseAddress = "http://130.56.253.23/api/";

        public void GetStoreItems()
        {
            StartCoroutine(RunGetStoreItems());
        }

        private IEnumerator RunGetStoreItems()
        {
            var www = UnityWebRequest.Get(string.Format("{0}store/items/getall", baseAddress));
            yield return www.Send();

            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Show results as text
                Debug.Log(www.downloadHandler.text);
                InterfaceHandlers.GetStoreItemsHandler(www.downloadHandler.text);
            }
        }
    }
}
