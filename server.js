/* Server.js
 *
 * Contains all the relevant serverside functionality for the game, including
 * CRUD of user accounts, scores, and items
 *
 * Authors: Phoebe Lew <plew@student.unimelb.edu.au><829493> and Kos Grillis <cgrillis@student.unimelb.edu><694699>
 *
 * For COMP30022 IT Project, semester 2, 2016
 */

var express = require("express");
var mysql = require('mysql');
var bodyParser = require('body-parser');

//If you don't need to use promises with mysql, use the following...
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'proj_admin',
    password: 'coolerdoves',
    database: 'ProjectLadder'
});

//If you need to use promises with mysql, use the following....
var mysqlPromise = require('promise-mysql');
var promiseConnection;
mysqlPromise.createConnection({
    host: 'localhost',
    user: 'proj_admin',
    password: 'coolerdoves',
    database: 'ProjectLadder'
}).then(function(conn){
    promiseConnection = conn;
});

var app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
}));

// TODO: Refactor to use pooled connections
connection.connect(function(err) {
    if (!err) {
        console.log("Database is connected ...");
    } else {
        console.log("Error connecting database");
    }
});

// **************************************** PLAYER CURRENCY *************************************************

/*
Functionality to retrieve the players current number of game coins.

DO NOT provide with null values. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to pass authenticated username.

Params:
    UserID - the ID of the user to retrieve details for. Should not be null.

Returns:
    A string representation of the number of game coins a user has. Please ensure
    to parse this correctly on the client side.
*/
app.get("/api/users/currency/retrieve/:UserID", function(req, res){

    var userID = req.params.UserID;

    connection.query('SELECT Currency FROM UserAccount WHERE UserID = ?', userID, function(err, resp){
        if (!err){
            //console.log(resp);
            var string = JSON.stringify(resp);
            var jsonResp = JSON.parse(string);
            res.send((jsonResp[0].Currency).toString());
        }
        else {
            res.send("-1");
        }
    });

    connection.end;
});

/*
Functionality to increase a players current number of game coins.

DO NOT provide with null values. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to pass authenticated username.

Params:
    UserID - the id of the user to add coins to
    NewCoins - an integer number of coins that will be added to the players current coins.

Returns:
    ER_INVALID_VALUES if the value of the coins is negative
    SUCCESS_UPDATE if no errors and coins are successfully added
*/
app.get("/api/users/:UserID/currency/add/:NewCoins", function(req, res){

    var newcoins;
    var addedcoins;
    var addedCoins = parseInt(req.params.NewCoins);

    if (addedCoins < 0){
        //someone is trying to decrement tsk tsk...
        res.send("ER_INVALID_VALUES");
    } else {

        var vals = [addedCoins, req.params.UserID];

        connection.query('UPDATE UserAccount SET Currency = Currency + ? WHERE UserID = ?', vals, function(err, resp){
            if(err){
                res.send(err);

            } else {
                res.send("SUCCESS_ADD");
            }
        });
    }
    connection.end;
});

// ****************************************** PLAYER ACCOUNTS *************************************************

/*
Functionality to register a new user.

POST format should be an x-www-form-urlencoded as follows:

    {
        "Username" : String,
        "Password" : String,
        "EmailAddress" : String,
        "FirstName" : String,
        "LastName" : String
    }

Params EmailAddress, FirstName and LastName allow null values, but must be sent
as "null" to the URL. (Note the string representation of null).

Returns:
    ER_DUP_ENTRY if specified username already exists in the database
    SUCCESS_REGISTRATION if the user is successfully added to the database
*/
app.post("/api/users/add/", function(req, res) {

    var body = req.body;
    for (var param in body) {
        if (body[param] == "null") {
            body[param] = null;
        }
    }

    var vals = [req.body.UserName,
                req.body.Password,
                req.body.EmailAddress,
                req.body.FirstName,
                req.body.LastName];

    var queryString = `INSERT INTO UserAccount (UserName, Password, EmailAddress, FirstName, LastName, Currency)
                       VALUES (?, ?, ?, ?, ?, 0)`;

    connection.query(queryString, vals, function(err, resp) {
        if (err) {
            res.send(err.code);
        } else {
            res.send("SUCCESS_REGISTRATION");
        }
    });
    connection.end;
});

/*
Functionality to authenticate a user.

POST format should be an x-www-form-urlencoded as follows:

    {
        "UserName" : String,
        "Password" : String
    }

Params do not accept null valus. Do not provide with a null username.

Returns:
    ER_INCORRECT_USERNAME if the specified username does not exist in the database
    UserID if the specified user is authenticated succesfully
    FAILURE_AUTHENTICATED if the user is not authenticated
*/
app.post("/api/users/authenticate/", function(req, res) {

    var queryString = `SELECT UserID, Password FROM UserAccount WHERE UserName = ?`;

    console.log("test");
    console.log(req.body.UserName);
    console.log(req.body.Password);

    connection.query(queryString, [req.body.UserName], function(err, resp) {

        if (!resp[0]) {
            res.send("ER_INCORRECT_USERNAME");
        } else {

            var string = JSON.stringify(resp);
            var jsonResp = JSON.parse(string);

            if (jsonResp[0].Password == req.body.Password) {
                res.send(jsonResp[0].UserID.toString());
            } else {
                res.send("FAILURE_AUTHENTICATED");
            }
        }
    });
    connection.end;
});

/*
Functionality to deregister a user.

POST format should be an x-www-form-urlencoded as follows:

    {
        "UserName" : String
    }

Params do not accept null valus. Do not provide with a null username. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to POST authenticated username.

Returns:
    SUCCESS_DEREGISTRATION if the user is succesfully deleted from the database.
*/
app.get("/api/users/remove/:UserID", function(req, res) {

    connection.query('DELETE FROM UserAccount WHERE UserID = ?', [req.params.UserID], function(err, resp){
        if (!err){
            res.send("SUCCESS_DEREGISTRATION");
        }
        else {
            res.send(err);
        }
    });
});

/*
Functionality to retreive a player's details.

Params:
    UserID - The ID of the user that the details are requested for. Should not be null.

Returns:
    A JSON object representation of the players details in the following format:

    {
        "UserName" : String,
        "EmailAddress" : String,
        "FirstName" : String,
        "LastName" : String,
        "Currency" : Integer
    }
*/
app.get("/api/users/details/:UserID/", function(req, res) {

    var queryString = `SELECT UserName, EmailAddress, FirstName, LastName, Currency FROM UserAccount WHERE UserID = ?`;

    connection.query(queryString, [req.params.UserID], function (err, rows) {
        if (!err) {
            var jsonRows = JSON.stringify(rows[0]);
            var parsedRows = JSON.parse(jsonRows);
            res.send(parsedRows);
        }
        else {
            res.send("error");
        }
    });
});

/*
Function to update a user's email address, first name and last name.

Refrain from providing null values, the function does not check the values are valid.

POST format should be an x-www-form-urlencoded as follows:

    {
        "UserID" : Integer,
        "Password" : String,
        "EmailAddress" : String,
        "FirstName" : String,
        "LastName" : String
    }

Note: UserID and password are required to ensure the correct user is being updated.
EmailAddress, FirstName and LastName are the new values that will be updated. The params are not checked on
the serverside, please ensure that they are valid on the client side.

Returns:
    ER_INCORRECT_USERNAME if the username given does not exist in the database
    FAILURE_AUTHENTICATED if the username exists in the database but the provided password is incorrect
    SUCCESS_UPDATE if the players details are successfully updated.
*/
app.post("/api/users/update/", function (req, res) {

    //First authenticate user...
    connection.query('SELECT UserID, Password FROM UserAccount WHERE UserID = ?', [req.body.UserID], function(err, resp) {
        if (!resp[0]) {
            res.send("ER_INCORRECT_USERID");
            connection.end();
            return;
        } else {
            var string = JSON.stringify(resp);
            var jsonResp = JSON.parse(string);
            if (jsonResp[0].Password != req.body.Password) {
                res.send("FAILURE_AUTHENTICATED");
                connection.end();
                return;
            }
        }
    });

    var vals = [req.body.EmailAddress, req.body.FirstName, req.body.LastName, req.body.UserID];
    var queryString = 'UPDATE UserAccount SET EmailAddress = ?, FirstName = ?, LastName = ? WHERE UserID = ?';

    //Perform update...
    connection.query(queryString, vals, function(err, resp) {
         if (err) {
            res.send(err.code);
        } else {
            res.send("SUCCESS_UPDATE");
        }
    });
});

/*
Function to change a player's username.

POST format should be an x-www-form-urlencoded as follows:

    {
        "UserID" : Integer,
        "Password" : String,
        "NewUserName" : String
    }

Do not provide null values. The server does not check if the values sent are null.

Returns:
    ER_DUP_ENTRY if the new requested username already exists in the database.
    ER_INCORRECT_USERNAME if the username given does not exist in the database
    FAILURE_AUTHENTICATED if the username exists in the database but the provided password is incorrect
    SUCCESS_USERNAME_UPDATE if the players username is successfully updated.
*/
app.post("/api/users/updateUsername/", function (req, res) {

    connection.query('SELECT UserId, Password FROM UserAccount WHERE UserID= ?', [req.body.UserID], function(err, resp) {
        if (!resp[0]) {
            res.send("ER_INCORRECT_USERNAME");
            return;
        } else {

            var string = JSON.stringify(resp);
            var jsonResp = JSON.parse(string);

            if (jsonResp[0].Password != req.body.Password) {
                res.send("FAILURE_AUTHENTICATED");
            }
        }
    });

    var vals = [req.body.NewUserName, req.body.UserID];
    var queryString = 'UPDATE UserAccount SET UserName = ? WHERE UserID = ?';

    connection.query(queryString, vals, function(err, resp) {
         if (err) {
            res.send(err.code);
        } else {
            res.send("SUCCESS_USERNAME_UPDATE");
        }
    });
});

/*
Function to change a player's password.

POST format should be an x-www-form-urlencoded as follows:

    {
        "UserID" : Integer,
        "Password" : String,
        "NewPassword" : String
    }

Do not provide null values. The server does not check if the values sent are null.

Returns:
    ER_INCORRECT_USERNAME if the provided password already exists in the database
    FAILURE_AUTHENTICATED if the provided password is wrong
    SUCCESS_PASSWORD_UPDATE if the password is successfully changed
*/
app.post("/api/users/updatePassword/", function (req, res) {

    connection.query('SELECT UserId, Password FROM UserAccount WHERE UserID = ?', [req.body.UserID], function(err, resp) {
        if (!resp[0]) {
            res.send("ER_INCORRECT_USERNAME");
        } else {

            var string = JSON.stringify(resp);
            var jsonResp = JSON.parse(string);

            if (jsonResp[0].Password != req.body.Password) {
                res.send("FAILURE_AUTHENTICATED");
            }
        }
    });

    var vals = [req.body.NewPassword, req.body.UserID];
    var queryString = 'UPDATE UserAccount SET Password = ? WHERE UserID = ?';

    connection.query(queryString, vals, function(err, resp) {
         if (err) {
            res.send(err.code);
        } else {
            res.send("SUCCESS_PASSWORD_UPDATE");
        }
    });
});

// ****************************************** STORE ENDPOINTS *************************************************

/*
Function to get all store items.

Returns:
    A JSON array of all items in the following format:

    [
        {
            "ItemID" : Integer,
            "Name" : String,
            "Description" : String,
            "ImagePath" : String,
            "Price" : Integer
        },
        ...
    ]
*/
app.get("/api/store/items/getall", function (req, res) {

    connection.query('SELECT * FROM StoreItem', function(err, rows) {
        if (err) {
            res.send("error");
        } else {
            var jsonRows = JSON.stringify(rows);
            var parsedRows = JSON.parse(jsonRows);
            res.send(parsedRows);
        }
    });
});


// ****************************************** PLAYER ITEMS    *************************************************

/*
Functionality to add a store bought item to a users account.

Does three things:
    1. Checks that the user has enough coins to buy the item
    2. If enough, subtracts the cost of the item from the users coins
    3. Adds the item to the users inventory table (UserInventory)

DO NOT provide with null values. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to pass authenticated user ID.

Params:
    UserID - The ID of the user that is buying an item. Should not be null.
    ItemID - The ID of the item that the user wants to buy. Should not be null.

Returns:
    ER_INVALID_ITEM if the wrong item ID is somehow passed into the function
    ER_NOT_ENOUGH_COINS if the player does not have enough coins to buy this item
    SUCCESS_BUY_ITEM if the item is successfully added to the players inventory
*/
app.get("/api/users/:UserID/items/buy/:ItemID", function(req, res){

    var itemPrice, userCoins, userID, itemID;

    userID = parseInt(req.params.UserID);
    itemID = parseInt(req.params.ItemID);
    var queryString = "SELECT Price FROM StoreItem WHERE ItemID = ?";

    console.log("Getting the price of itemID " + itemID + "...");
    promiseConnection.query(queryString, itemID).then(function(resp){
        var resp_ = resp[0];
        return resp_;
    }).then(function(resp_){

        var itemPrice = parseInt(resp_.Price);
        var queryString2 = "SELECT Currency FROM UserAccount WHERE UserID = ?";

        promiseConnection.query(queryString2, userID).then(function(resp2){
            var resp2_ = resp2[0];
            return resp2_;
        }).then(function(resp2_){
            var userCoins = parseInt(resp2_.Currency);
        }).then(function(itemprice, usercurrency){
            //console.log(itemPrice);
            if (itemPrice > userCoins){
                console.log("Error: player doesn't have enough coins");
                res.send("ER_NOT_ENOUGH_COINS");
            } else {
                connection.query('UPDATE UserAccount SET Currency = Currency - ? WHERE UserID = ?', [itemPrice, userID], function(err, resp){
                    //massive error...
                    if (err){
                        res.send(err);
                    }
                });
                console.log("Add item into user inventory...");
                connection.query('INSERT INTO InventoryItem (UserFK, ItemFK) VALUES (?, ?)', [userID, itemID] , function(err, resp){
                    if (!err){
                        res.send("SUCCESS_BUY_ITEM");
                    }
                });
            }
        });
    })
});

/*
Functionality to remove an item from a players inventory. This should be called
when the player uses an item in game.

DO NOT provide with null values. Assumes that the user has already been authenticated,
and does not do any checks for itself. Hence ensure to pass authenticated user ID.

Params:
    UserID - The ID of the user that should have the item removed from their inventory. Should not be null.
    ItemID - The ID of the item that should be removed from the above users inventory. Should not be null.

Returns:
    SUCCESS_ITEM_DELETED if the item is successfully deleted from the players inventory
*/
app.get("/api/users/:UserID/items/remove/:ItemID", function(req, res){

    var vals;

    vals = [parseInt(req.params.UserID), parseInt(req.params.ItemID)];

    connection.query('DELETE FROM InventoryItem WHERE UserFK = ? AND ItemFK = ? LIMIT 1', vals, function(err, resp){
        if (!err){
            res.send("SUCCESS_ITEM_DELETED");
        } else {
            res.send(err);
        }
    });
    connection.end;
});

/*
Function to get all of a player's items
*/
app.get("/api/users/items/get/:UserID", function (req, res) {

    var queryString = `SELECT StoreItem.Name, StoreItem.Description, StoreItem.ImagePath, COUNT(*)
                       FROM InventoryItem
                       JOIN StoreItem ON InventoryItem.ItemFK = StoreItem.ItemID
                       WHERE InventoryItem.UserFK = ?
                       GROUP BY StoreItem.Name`;

    var vals = [req.params.userID];
    connection.query(queryString, vals, function(err, rows) {
        if (err) {
            res.send("error");
        } else {

            var jsonRows = JSON.stringify(rows);
            var parsedRows = JSON.parse(jsonRows);
            res.send(jsonRows);
        }
    });
});

// ****************************************** SCOREBOARD ENDPOINTS ********************************************

/*
Function to update a players score.

Params:
    userID - the ID of the user to be updated
    scoredPoints - an integer value representing that latest number of points the user has scored

Adds scoredPoints to the current score of userID and updates the date. If the user does not exist
in the ScoreLog table, a new row for that user is inserted.

Returns:
    SUCCESS_UPDATE_SCORE if the score is updated successfully
*/
app.get("/api/scoreboard/update/:userID/:scoredPoints/", function (req, res) {

    //Check if the player has a row in the scoreboard column...
    var queryString = "SELECT ScoreLogID FROM ScoreLog WHERE UserFK = ?";
    var vals = [req.params.userID];

    promiseConnection.query(queryString, req.params.userID).then(function(resp){

        if (resp[0]){

            //Update the players score...
            queryString = "UPDATE ScoreLog SET DateTime = now(), ScoredPoints = ScoredPoints + ? WHERE UserFK = ?";
            vals = [req.params.scoredPoints, req.params.userID];

            connection.query(queryString, vals, function(err, resp){
                if (err) {
                    res.send(err.code);
                } else {
                    res.send("SUCCESS_UPDATE_SCORE");
                }
            });
        } else {

            //add a new row for the player...
            queryString = "INSERT INTO ScoreLog (DateTime, ScoredPoints, UserFK) VALUES(now(), ?, ?)";
            vals = [req.params.scoredPoints, req.params.userID];

            connection.query(queryString, vals, function(err, resp){
                if (err) {
                    res.send(err.code);
                } else {
                    res.send("SUCCESS_UPDATE_SCORE");
                }
            });
        }
    });
});

/*
Function to get all scores sorted by descending order.

Returns:
    A JSON array of all scores in the following format:

    [
        {
            "UserName": 19,
            "ScoredPoints": 20
        }
        ...
    ]

*/
app.get("/api/scoreboard/getScores/", function (req, res) {

    var queryString = `SELECT UserAccount.UserName, ScoreLog.ScoredPoints 
                        FROM ScoreLog
                        JOIN UserAccount ON ScoreLog.UserFK = UserAccount.UserID
                        ORDER BY ScoredPoints 
                        DESC LIMIT 5`;

    connection.query(queryString, function(err, rows) {
        if (err) {
            res.send("error");
        } else {
            var jsonRows = JSON.stringify(rows);
            var parsedRows = JSON.parse(jsonRows);
            res.send(parsedRows);
        }
    });
});

/*
Function to get a single players score.

Returns:
    A JSON object in the following format:

    {
        "UserID": 19,
        "ScoredPoints": 20
    }

*/
app.get("/api/users/scoreboard/get/:userID", function (req, res) {

    var queryString = `SELECT UserAccount.UserName, ScoredPoints
                        FROM ScoreLog  
                        JOIN UserAccount ON ScoreLog.UserFK = UserAccount.UserID
                        AND UserAccount.UserID = 19
                        ORDER BY ScoredPoints 
                        DESC LIMIT 5`;

    connection.query(queryString, [req.params.userID], function (err, rows) {
        if (err) {
            res.send("error");
        } else {
            var jsonRows = JSON.stringify(rows[0]);
            var json = JSON.parse(jsonRows);
            res.send(json);
        }
    });
});


// ****************************************** CHAT ENDPOINTS  *************************************************

/*
Functionality to send a message to the server.

POST format should be an x-www-form-urlencoded as follows:

    {
        "from": int,
        "to": int,
        "subject": string,
        "message": string
    }

Do not provide null values. The server does not check if the values sent are null.

Returns:
    SUCCESS_SEND_MESSAGE if the message is successfully receieved and saved to the mysql connection.
    else sends an error code.
*/
app.post("/api/messages/sendmessage", function(req, res) {

    var queryString = `INSERT INTO Message
                      (SenderID, ReceiverID, Timestamp, SubjectLine, MessageContent, ResRead, SenderDeleted, ResDeleted)
                      VALUES(?, ?, NOW(), ?, ?, FALSE, FALSE, FALSE)`;

    var vals = [req.body.from, req.body.to, req.body.subject, req.body.message];

    connection.query(queryString, vals, function(err, resp){
        if (err){
            res.send(err.code);
        } else {
            res.send("SUCCESS_SEND_MESSAGE");
        }
    });
});

/*
Functionality to check if a user has unread messages

Params:
    UserID - the ID of the user to check unread messages for

Returns:
    TRUE_UNREAD_MESSAGES if there are unread messages
    FALSE_UNREAD_MESSAGES if there are no unread messages
*/
app.get("/api/users/:UserID/checkmessages", function(req, res){

    connection.query('SELECT * FROM Message WHERE ReceiverID = ? AND ResRead = FALSE', parseInt(req.params.UserID), function(err, resp){
        if (err){
            res.send(err);
        } else {
            if(resp[0]){
                res.send("TRUE_UNREAD_MESSAGES");
            } else {
                res.send("FALSE_UNREAD_MESSAGES");
            }
        }
    });
});

/*
Functionality to mark a message as read

Params:
    MessageID - the ID of the message to mark as read by the receiver

Note that there is only one parameter for the messageID. This is fine because the message
will be marked as read for the receiver, of which there is only one for a particular message.

Returns:
    SUCCESS_MARKED_READ if the specified message has been marked as read
*/
app.get("/api/messages/markread/:MessageID", function(req, res){

    connection.query("UPDATE Message SET ResRead = TRUE WHERE MessageID = ?", parseInt(req.params.MessageID), function(err, resp){
        if (err){
            res.send(err);
        } else {
            res.send("SUCCESS_MARKED_READ");
        }
    });
});

/*
Functionality to delete a message

Params:
    UserID - the ID of the user which deleted the message
    MessageID - the ID of the message which was deleted by the user with ID UserID

Note that 'deleted' simply means hidden from the user on the client side. deleted
messages still persist in the DB, and are marked as "deleted".

Returns:
    SUCCESS_DELETED if the specified message is "deleted" with no errors
*/
app.get("/api/users/:UserID/messages/delete/:MessageID", function(req, res){

    var messageID = parseInt(req.params.MessageID);
    var userID = parseInt(req.params.UserID);
    var queryString = "SELECT MessageID, SenderID, ReceiverID FROM Message WHERE MessageID = ?";

    promiseConnection.query(queryString, messageID).then(function(resp){
        var message = resp[0];
        return message;
    }).then(function(message){
        if (message.SenderID === userID){

            //mark sender deleted...
            queryString = "UPDATE Message SET SenderDeleted = TRUE WHERE MessageID = ? AND SenderID = ?";
            connection.query(queryString, [message.MessageID, message.SenderID], function(err, respo){
                if (!err){
                    res.send("SUCCESS_DELETED");
                }
            });

        } else if (message.ReceiverID === userID){

            //mark receiver deleted...
            queryString = "UPDATE Message SET ResDeleted = TRUE WHERE MessageID = ? AND ReceiverID = ?";
            connection.query(queryString, [message.MessageID, message.ReceiverID], function(err, respo){
                if (!err){
                    res.send("SUCCESS_DELETED");
                }
            });
        }
        connection.end();
    });
    promiseConnection.end();
});

/*
Functionality to get all of a users unread messages.

Params:
    :UserID The receipient of the unread messages

Returns:
    A JSON array in the following format (One json object per unread message):

    [
        {
            "MessageID": 12,
            "SenderID": 19,
            "ReceiverID": 20,
            "Timestamp": "2016-09-19T01:04:05.000Z",
            "SubjectLine": "Test message",
            "MessageContent": "This is a test message content"
        },
        ...
    ]
*/
app.get("/api/users/:UserID/messages/get/unread", function(req, res){

    var userID = parseInt(req.params.UserID);
    var queryString = `SELECT MessageID, SenderID, ReceiverID, Timestamp, SubjectLine, MessageContent
                       FROM Message WHERE ReceiverID = ? AND ResRead = FALSE AND ResDeleted = FALSE`;

    connection.query(queryString, userID, function(err, resp){
        if (!err){
            var jsonRows = JSON.stringify(resp);
            var json = JSON.parse(jsonRows);
            res.send(json);
        }
    });
});

/*
Functionality to get all of a users sent messages (provided they are not deleted).

Params:
    :UserID The receipient of the unread messages

Returns:
    A JSON array in the following format (One json object per unread message):

    [
        {
            "MessageID": 12,
            "SenderID": 19,
            "ReceiverID": 20,
            "Timestamp": "2016-09-19T01:04:05.000Z",
            "SubjectLine": "Test message",
            "MessageContent": "This is a test message content"
        },
        ...
    ]
*/
app.get("/api/users/:UserID/messages/get/sent", function(req, res){

    var userID = parseInt(req.params.UserID);
    var queryString = `SELECT MessageID, SenderID, ReceiverID, Timestamp, SubjectLine, MessageContent
                       FROM Message WHERE SenderID = ? AND SenderDeleted = FALSE`;

    connection.query(queryString, userID, function(err, resp){
        if (!err){
            var jsonRows = JSON.stringify(resp);
            var json = JSON.parse(jsonRows);
            res.send(json);
        }
    });
});

/*
Functionality to get all of a users received messages (provided they are not deleted).

Params:
    :UserID The receipient of the unread messages

Returns:
    A JSON array in the following format (One json object per unread message):

    [
        {
            "MessageID": 12,
            "SenderID": 19,
            "ReceiverID": 20,
            "Timestamp": "2016-09-19T01:04:05.000Z",
            "SubjectLine": "Test message",
            "MessageContent": "This is a test message content"
        },
        ...
    ]
*/
app.get("/api/users/:UserID/messages/get/received", function(req, res){

    var userID = parseInt(req.params.UserID);
    var queryString = `SELECT MessageID, SenderID, ReceiverID, Timestamp, SubjectLine, MessageContent
                       FROM Message WHERE ReceiverID = ? AND ResDeleted = FALSE`;

    connection.query(queryString, userID, function(err, resp){
        if (!err){
            var jsonRows = JSON.stringify(resp);
            var json = JSON.parse(jsonRows);
            res.send(json);
        }
    });
});

app.listen(80, function() {
	console.log('Server listening on port 80');
});

